package ro.personalprojects.QuizGame;


	
	import java.awt.Color;

	import java.awt.Container;
	import java.awt.Font;
	import java.awt.event.ActionEvent;
	import java.awt.event.ActionListener;

	import javax.swing.ButtonGroup;
	import javax.swing.JButton;
	import javax.swing.JFrame;
	import javax.swing.JLabel;
	import javax.swing.JOptionPane;
	import javax.swing.JRadioButton;
	import javax.swing.WindowConstants;

	public class QuizGame implements ActionListener {

		JFrame frame;
		JRadioButton radiob1, radiob2, radiob3, radiob4;
		JButton b1, b2;
		JLabel label1, label2;

		ButtonGroup buttong;

		String question[] = { "The founder of Microsoft is..." };
		String op1[] = { "Bill Gates" };
		String op2[] = { "Steve Jobs" };
		String op3[] = { "Jeff Bezos" };
		String op4[] = { "Mark Zukenberg" };
		String answer[] = { "Bill Gates" };

		int cn;

		QuizGame() {

			// Set main frame

			frame = new JFrame();
			frame.setLayout(null);
			frame.setSize(600, 600);
			Container cont = frame.getContentPane();
			cont.setBackground(Color.yellow);

			label1 = new JLabel(question[0]);
			label1.setBounds(100, 50, 600, 30);
			frame.add(label1);
			label1.setFont(new Font("comic", Font.BOLD, 25));

			frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			// Set radio buttons

			radiob1 = new JRadioButton(op1[0]);
			radiob1.setBounds(100, 200, 150, 30);
			frame.add(radiob1);

			radiob2 = new JRadioButton(op2[0]);
			radiob2.setBounds(100, 120, 150, 30);
			frame.add(radiob2);

			radiob3 = new JRadioButton(op3[0]);
			radiob3.setBounds(350, 120, 150, 30);
			frame.add(radiob3);

			radiob4 = new JRadioButton(op4[0]);
			radiob4.setBounds(350, 200, 150, 30);
			frame.add(radiob4);

			// Set Button group....

			buttong = new ButtonGroup();

			buttong.add(radiob1);
			buttong.add(radiob2);
			buttong.add(radiob3);
			buttong.add(radiob4);

			radiob1.addActionListener(this);
			radiob2.addActionListener(this);
			radiob3.addActionListener(this);
			radiob4.addActionListener(this);

			// set submit and next button

			b1 = new JButton("Submit");
			b1.setBounds(100, 400, 100, 30);
			frame.add(b1);

			b2 = new JButton("Next");
			b2.setBounds(250, 400, 100, 30);
			frame.add(b2);

			b1.addActionListener(this);
			b2.addActionListener(this);
			frame.setVisible(true);

		}
		
		
	// start the game
		
		public static void main(String s[]) {

			new QuizGame();

		}

		public void actionPerformed(ActionEvent e) {

			
			// conditions & validate the answer
			
			if (e.getSource() == b1) {

				String en = "";
				
				if(radiob1.isSelected())
					en = radiob1.getText();
				if(radiob2.isSelected())
					en = radiob2.getText();
				if(radiob3.isSelected())
					en = radiob3.getText();
				if(radiob4.isSelected())
					en = radiob4.getText();
				if(en.equals(answer[cn]))
					JOptionPane.showMessageDialog(null, "Right Answer");
				else
					JOptionPane.showMessageDialog(null, "Wrong Answer");
			}
			if (e.getSource() == b2)
			{
				cn++;
				
				label1.setText(question[cn]);
				radiob1.setText(op1[cn]);
				radiob2.setText(op2 [cn]);
				radiob3.setText(op3[cn]);
				radiob4.setText(op4 [cn]);
				
				
				
			}
		}
	}



